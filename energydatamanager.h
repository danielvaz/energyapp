#ifndef ENERGYDATAMANAGER_H
#define ENERGYDATAMANAGER_H

#include <memory>

#include <QtQml>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QVariantList>

#include "thingspeakrequest.h"
#include "abstracttable.h"
#include "dailymeasurement.h"
#include "dailymeasurementlistmodel.h"
#include "homemeasurement.h"
#include "sqlitedb.h"

class EnergyDataManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double current READ current NOTIFY currentChanged)
    Q_PROPERTY(double temperature READ temperature NOTIFY temperatureChanged)
    Q_PROPERTY(double luminosity READ luminosity NOTIFY luminosityChanged)
    Q_PROPERTY(QDateTime ts READ ts NOTIFY tsChanged)
    Q_PROPERTY(QList<double> currents READ currents NOTIFY currentsChanged)
    Q_PROPERTY(QList<double> temperatures READ temperatures NOTIFY temperaturesChanged)
    Q_PROPERTY(QList<double> luminosities READ luminosities NOTIFY luminositiesChanged)
    Q_PROPERTY(QVariantList timestamp READ timestamp NOTIFY timestampChanged)
    Q_PROPERTY(double minCurrent READ minCurrent NOTIFY minCurrentChanged)
    Q_PROPERTY(double maxCurrent READ maxCurrent NOTIFY maxCurrentChanged)
    Q_PROPERTY(double minTemperature READ minTemperature NOTIFY minTemperatureChanged)
    Q_PROPERTY(double maxTemperature READ maxTemperature NOTIFY maxTemperatureChanged)
    Q_PROPERTY(double minLuminosity READ minLuminosity NOTIFY minLuminosityChanged)
    Q_PROPERTY(double maxLuminosity READ maxLuminosity NOTIFY maxLuminosityChanged)
    Q_PROPERTY(double powerConsumption READ powerConsumption NOTIFY powerConsumptionChanged)
    Q_PROPERTY(double voltage READ voltage WRITE setVoltage NOTIFY voltageChanged)
    Q_PROPERTY(double energyTE READ energyTE WRITE setEnergyTE NOTIFY energyTEChanged)
    Q_PROPERTY(double energyTUSD READ energyTUSD WRITE setEnergyTUSD NOTIFY energyTUSDChanged)
    Q_PROPERTY(DailyMeasurementListModel* dailyData READ dailyData NOTIFY dailyDataChanged)
    Q_PROPERTY(ThingspeakRequest* api READ api CONSTANT)

public:
    explicit EnergyDataManager(QObject *parent = 0);

    Q_INVOKABLE void getInstantaneousData();

    Q_INVOKABLE void requestLastDays(int days);

    static int registerQml()
    {
        return qmlRegisterType<EnergyDataManager>("energy", 1, 0, "EnergyDataManager");
    }

    double current() const
    {
        return m_current;
    }

    double temperature() const
    {
        return m_temperature;
    }

    double luminosity() const
    {
        return m_luminosity;
    }

    QDateTime ts() const
    {
        return m_ts;
    }

    QList<double> currents() const
    {
        return m_currents;
    }

    QList<double> temperatures() const
    {
        return m_temperatures;
    }

    QList<double> luminosities() const
    {
        return m_luminosities;
    }

    QVariantList timestamp() const
    {
        return m_timestamp;
    }
    double minCurrent() const
    {
        return m_minCurrent;
    }

    double maxCurrent() const
    {
        return m_maxCurrent;
    }

    double minTemperature() const
    {
        return m_minTemperature;
    }

    double maxTemperature() const
    {
        return m_maxTemperature;
    }

    double minLuminosity() const
    {
        return m_minLuminosity;
    }

    double maxLuminosity() const
    {
        return m_maxLuminosity;
    }

    double powerConsumption() const
    {
        return m_powerConsumption;
    }

    double voltage() const
    {
        return m_voltage;
    }

    double energyTE() const
    {
        return m_energyTE;
    }

    double energyTUSD() const
    {
        return m_energyTUSD;
    }

    DailyMeasurementListModel* dailyData()
    {
        return &m_dailyData;
    }

    void setCurrent(double current)
    {
        if (m_current == current)
            return;

        m_current = current;
        emit currentChanged(current);
    }

    void setTemperature(double temperature)
    {
        if (m_temperature == temperature)
            return;

        m_temperature = temperature;
        emit temperatureChanged(temperature);
    }

    void setLuminosity(double luminosity)
    {
        if (m_luminosity == luminosity)
            return;

        m_luminosity = luminosity;
        emit luminosityChanged(luminosity);
    }

    void setTs(QString ts)
    {
        QDateTime aux = QDateTime::fromString(ts, "yyyy-MM-ddThh:mm:ssZ").addSecs(-10800);
        if (m_ts == aux)
            return;

        m_ts = aux;
        emit tsChanged(aux);
    }

    void setMinCurrent(double minCurrent)
    {
        if (m_minCurrent == minCurrent)
            return;

        m_minCurrent = minCurrent;
        emit minCurrentChanged(minCurrent);
    }

    void setMaxCurrent(double maxCurrent)
    {
        if (m_maxCurrent == maxCurrent)
            return;

        m_maxCurrent = maxCurrent;
        emit maxCurrentChanged(maxCurrent);
    }

    void setMinTemperature(double minTemperature)
    {
        if (m_minTemperature == minTemperature)
            return;

        m_minTemperature = minTemperature;
        emit minTemperatureChanged(minTemperature);
    }

    void setMaxTemperature(double maxTemperature)
    {
        if (m_maxTemperature == maxTemperature)
            return;

        m_maxTemperature = maxTemperature;
        emit maxTemperatureChanged(maxTemperature);
    }

    void setMinLuminosity(double minLuminosity)
    {
        if (m_minLuminosity == minLuminosity)
            return;

        m_minLuminosity = minLuminosity;
        emit minLuminosityChanged(minLuminosity);
    }

    void setMaxLuminosity(double maxLuminosity)
    {
        if (m_maxLuminosity == maxLuminosity)
            return;

        m_maxLuminosity = maxLuminosity;
        emit maxLuminosityChanged(maxLuminosity);
    }

    void setPowerConsumption(double powerConsumption)
    {
        if (m_powerConsumption == powerConsumption)
            return;

        m_powerConsumption = powerConsumption;
        emit powerConsumptionChanged(powerConsumption);
    }

    void setVoltage(double voltage)
    {
        if (m_voltage == voltage)
            return;

        m_voltage = voltage;
        emit voltageChanged(voltage);
    }

    void setEnergyTE(double energyTE)
    {
        if (m_energyTE == energyTE)
            return;

        m_energyTE = energyTE;
        emit energyTEChanged(energyTE);
    }

    void setEnergyTUSD(double energyTUSD)
    {
        if (m_energyTUSD == energyTUSD)
            return;

        m_energyTUSD = energyTUSD;
        emit energyTUSDChanged(energyTUSD);
    }

    ThingspeakRequest* api() const
    {
        return m_api;
    }

signals:
    void currentChanged(double current);
    void temperatureChanged(double temperature);
    void luminosityChanged(double luminosity);
    void tsChanged(QDateTime ts);
    void currentsChanged();
    void temperaturesChanged();
    void luminositiesChanged();
    void timestampChanged();
    void minTemperatureChanged(double minTemperature);
    void maxTemperatureChanged(double maxTemperature);
    void minCurrentChanged(double minCurrent);
    void maxCurrentChanged(double maxCurrent);
    void minLuminosityChanged(double minLuminosity);
    void maxLuminosityChanged(double maxLuminosity);
    void powerConsumptionChanged(double powerConsumption);
    void voltageChanged(double voltage);
    void energyTEChanged(double energyTE);
    void energyTUSDChanged(double energyTUSD);
    void dailyDataChanged();

private:
    double m_current;
    double m_temperature;
    double m_luminosity;
    QDateTime m_ts;
    QList<double> m_currents;
    QList<double> m_temperatures;
    QList<double> m_luminosities;
    QVariantList m_timestamp;
    double m_minCurrent;
    double m_maxCurrent;
    double m_minTemperature;
    double m_maxTemperature;
    double m_minLuminosity;
    double m_maxLuminosity;
    double m_powerConsumption;
    double m_voltage;
    double m_energyTE;
    double m_energyTUSD;
    std::unique_ptr< AbstractTable<HomeMeasurement> > m_measurementsTable;
    std::unique_ptr< AbstractTable<DailyMeasurement> > m_dailyTable;
    SqliteDB m_db;
    DailyMeasurementListModel m_dailyData;
    ThingspeakRequest* m_api;
    int m_daysToRequest;
    bool m_requestingMonthly;
    //
    void instantaneousDataReady(const QJsonDocument &doc);
    void dailyConsumptionReady(const QJsonDocument &doc);
    //
    void getDailyConsumption();
    void storePreviousData(const QJsonDocument &doc);
};

#endif // ENERGYDATAMANAGER_H
