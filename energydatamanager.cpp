#include "energydatamanager.h"

EnergyDataManager::EnergyDataManager(QObject *parent)
    : QObject(parent)
    , m_current(0.0)
    , m_temperature(0.0)
    , m_luminosity(0.0)
    , m_minCurrent(0.0)
    , m_maxCurrent(0.0)
    , m_minTemperature(0.0)
    , m_maxTemperature(10.0)
    , m_minLuminosity(0.0)
    , m_maxLuminosity(0.0)
    , m_powerConsumption(0.0)
    , m_voltage(127.0)
    , m_energyTE(0.1754800)
    , m_energyTUSD(0.2717100)
    , m_db(QSqlDatabase::addDatabase("QSQLITE"))
    , m_api(new ThingspeakRequest(this))
    , m_daysToRequest(0)
    , m_requestingMonthly(false)
{
    m_db.setDatabaseName("data.sqlite");
    m_db.openDB();
    m_measurementsTable.reset(new AbstractTable<HomeMeasurement>("yesterday", m_db));
    m_dailyTable.reset(new AbstractTable<DailyMeasurement>("daily", m_db));
}

void EnergyDataManager::getInstantaneousData()
{
    m_api->instantaneousData();
    connect(m_api, &ThingspeakRequest::thingspeakResponseChanged,
            this, &EnergyDataManager::instantaneousDataReady);
}

void EnergyDataManager::requestLastDays(int days)
{
    if ( days && !m_requestingMonthly ) {
        m_requestingMonthly = true;
        m_dailyData.clear();
    }
    if ( days > 0 ) {
        m_daysToRequest = days;
        QDateTime d0 = QDateTime(QDate::currentDate().addDays(-m_daysToRequest));
        if ( m_dailyTable->selectLike("date(day)", d0.toString("yyyy-MM-dd")) ) {
            DailyMeasurement *m = m_dailyTable->getNextDAO();
            while ( m != Q_NULLPTR && m_daysToRequest > 0) {
                m_dailyData.insertRow(DailyMeasurementData(m));
                m_dailyTable->closeSelect();
                delete m;
                --m_daysToRequest;
                d0 = QDateTime(QDate::currentDate().addDays(-m_daysToRequest));
                m_dailyTable->selectLike("date(day)", d0.toString("yyyy-MM-dd"));
                m = m_dailyTable->getNextDAO();
            }
            if ( m_daysToRequest ) {
                m_api->requestDay(d0, ThingspeakRequest::Avg10Minutes);
                connect(m_api, &ThingspeakRequest::thingspeakResponseChanged,
                        this, &EnergyDataManager::storePreviousData);
            }
        }
    }
    if( m_daysToRequest == 0 && m_requestingMonthly ) {
        dailyDataChanged();
        m_requestingMonthly = false;
    }
}

void EnergyDataManager::getDailyConsumption()
{
    m_api->requestCurrentDay(ThingspeakRequest::Avg10Minutes);
    connect(m_api, &ThingspeakRequest::thingspeakResponseChanged,
            this, &EnergyDataManager::dailyConsumptionReady);
}

void EnergyDataManager::storePreviousData(const QJsonDocument &doc)
{
    disconnect(m_api, &ThingspeakRequest::thingspeakResponseChanged,
               this, &EnergyDataManager::storePreviousData);
    QJsonObject obj = doc.object();
    QJsonArray feedArray = obj.value("feeds").toArray();
    QJsonObject feed;
    bool cok, tok, lok;
    QDateTime ts;
    double curr, temp, lumi;
    double avgCurr = 0.0, avgTemp = 0.0, avgLumi = 0.0;
    double power = 0.0;
    int length = feedArray.size();
    bool isYesterday = m_api->requestingDay().date() == QDate::currentDate().addDays(-1);
    DailyMeasurement d;
    if ( length ) {
        HomeMeasurement h;
        for(int i=0;i<length; ++i) {
            feed = feedArray.at(i).toObject();
            curr = feed.value("field1").toString().toDouble(&cok);
            temp = feed.value("field2").toString().toDouble(&tok);
            lumi = feed.value("field3").toString().toDouble(&lok);
            ts = QDateTime::fromString(feed.value("created_at").toString(), "yyyy-MM-ddThh:mm:ssZ").addSecs(-10800);
            if ( cok && tok && lok ) {
                if ( isYesterday ) {
                    h.setTimestamp(ts);
                    h.setCurrent(curr);
                    h.setTemperature(temp);
                    h.setLuminosity(lumi);
                    m_measurementsTable->persist(&h);
                }
                power += curr * m_voltage * 0.000166666666667;
                avgCurr += curr;
                avgTemp += temp;
                avgLumi += lumi;
            }
        }
        d.setDay(m_api->requestingDay());
        d.setPower(power);
        d.setAvgTemperature(avgTemp/length);
        d.setAvgLuminosity(avgLumi/length);
    } else {
        d.setDay(m_api->requestingDay());
        d.setPower(0.0);
        d.setAvgTemperature(0.0);
        d.setAvgLuminosity(0.0);
    }
    m_dailyData.insertRow(DailyMeasurementData(&d));
    m_dailyTable->persist(&d);
    --m_daysToRequest;
    requestLastDays(m_daysToRequest);
}

void EnergyDataManager::instantaneousDataReady(const QJsonDocument &doc) {
    disconnect(m_api, &ThingspeakRequest::thingspeakResponseChanged,
               this, &EnergyDataManager::instantaneousDataReady);
    QJsonObject obj = doc.object();
    QJsonArray feedArray = obj.value("feeds").toArray();
    QJsonObject feed = feedArray.at(0).toObject();
    setTs(feed.value("created_at").toString());
    setCurrent(feed.value("field1").toString().toDouble());
    setTemperature(feed.value("field2").toString().toDouble());
    setLuminosity(feed.value("field3").toString().toDouble());
    getDailyConsumption();
}

void EnergyDataManager::dailyConsumptionReady(const QJsonDocument &doc)
{
    qDebug() << Q_FUNC_INFO;
    disconnect(m_api, &ThingspeakRequest::thingspeakResponseChanged,
               this, &EnergyDataManager::dailyConsumptionReady);
    QJsonObject obj = doc.object();
    QJsonArray feedArray = obj.value("feeds").toArray();
    QDateTime now(QDateTime::currentDateTime());
    m_currents.clear();
    m_temperatures.clear();
    m_luminosities.clear();
    m_timestamp.clear();
    m_minCurrent = std::numeric_limits<double>::max();
    m_minTemperature = std::numeric_limits<double>::max();
    m_minLuminosity = std::numeric_limits<double>::max();
    m_maxCurrent = std::numeric_limits<double>::min();
    m_maxTemperature = std::numeric_limits<double>::min();
    m_maxLuminosity = std::numeric_limits<double>::min();
    m_temperatures.reserve(feedArray.size());
    m_timestamp.reserve(feedArray.size());
    bool cok, tok, lok;
    double curr, temp, lumi;
    double power = 0.0;
    QJsonObject feed;
    for(int i=0; i<feedArray.size(); ++i) {
        feed = feedArray.at(i).toObject();
        curr = feed.value("field1").toString().toDouble(&cok);
        temp = feed.value("field2").toString().toDouble(&tok);
        lumi = feed.value("field3").toString().toDouble(&lok);
        if ( cok && tok && lok ) {
            m_maxCurrent = std::max(curr, m_maxCurrent);
            m_maxTemperature = std::max(temp, m_maxTemperature);
            m_maxLuminosity = std::max(lumi, m_maxLuminosity);
            m_minCurrent = std::min(curr, m_minCurrent);
            m_minTemperature = std::min(temp, m_minTemperature);
            m_minLuminosity = std::min(lumi, m_minLuminosity);
            m_currents.append(curr);
            m_temperatures.append(temp);
            m_luminosities.append(lumi);
            m_timestamp.append(QDateTime::fromString(feed.value("created_at").toString(), "yyyy-MM-ddThh:mm:ssZ").addSecs(now.offsetFromUtc()));
            power += curr * m_voltage * 0.000166666666667;
        }
    }
    currentsChanged();
    temperaturesChanged();
    luminositiesChanged();
    timestampChanged();
    minCurrentChanged(m_minCurrent);
    maxCurrentChanged(m_maxCurrent);
    minTemperatureChanged(m_minTemperature);
    maxTemperatureChanged(m_maxTemperature);
    minLuminosityChanged(m_minLuminosity);
    maxLuminosityChanged(m_maxLuminosity);
    setPowerConsumption(power);
}
