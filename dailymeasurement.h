#ifndef DAILYMEASUREMENT_H
#define DAILYMEASUREMENT_H

#include <QDateTime>
#include <QObject>

class DailyMeasurement : public QObject {
    Q_OBJECT
    Q_PROPERTY(QDateTime day READ day WRITE setDay NOTIFY dateTimeChanged)
    Q_PROPERTY(double power READ power WRITE setPower NOTIFY powerChanged)
    Q_PROPERTY(double avgTemperature READ avgTemperature WRITE setAvgTemperature NOTIFY avgTemperatureChanged)
    Q_PROPERTY(double avgLuminosity READ avgLuminosity WRITE setAvgLuminosity NOTIFY avgLuminosityChanged)
public:
    DailyMeasurement(QObject *parent = Q_NULLPTR)
        : QObject(parent)
        , m_power(0.0)
        , m_avgTemperature(0.0)
        , m_avgLuminosity(0.0)
    {

    }
    ~DailyMeasurement() { }
    QDateTime day() const
    {
        return m_day;
    }

    double power() const
    {
        return m_power;
    }

    double avgTemperature() const
    {
        return m_avgTemperature;
    }

    double avgLuminosity() const
    {
        return m_avgLuminosity;
    }

    void setDay(QDateTime day)
    {
        if (m_day == day)
            return;

        m_day = day;
        emit dateTimeChanged(day);
    }

    void setPower(double power)
    {
        if (m_power == power)
            return;

        m_power = power;
        emit powerChanged(power);
    }

    void setAvgTemperature(double avgTemperature)
    {
        if (m_avgTemperature == avgTemperature)
            return;

        m_avgTemperature = avgTemperature;
        emit avgTemperatureChanged(avgTemperature);
    }

    void setAvgLuminosity(double avgLuminosity)
    {
        if (m_avgLuminosity == avgLuminosity)
            return;

        m_avgLuminosity = avgLuminosity;
        emit avgLuminosityChanged(avgLuminosity);
    }

signals:
    void dateTimeChanged(QDateTime day);
    void powerChanged(double power);
    void avgTemperatureChanged(double avgTemperature);
    void avgLuminosityChanged(double avgLuminosity);

private:
    QDateTime m_day;
    double m_power;
    double m_avgTemperature;
    double m_avgLuminosity;
};

#endif // DAILYMEASUREMENT_H
