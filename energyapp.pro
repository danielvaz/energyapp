TEMPLATE = app

QT += qml quick network charts widgets
CONFIG += c++11

SOURCES += main.cpp \
    homemeasurement.cpp \
    dailymeasurementlistmodel.cpp \
    dailymeasurementdata.cpp \
    energydatamanager.cpp

RESOURCES += qml.qrc

TARGET = EnergyMonitor

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    homemeasurement.h \
    dailymeasurement.h \
    listmodeltemplate.h \
    dailymeasurementlistmodel.h \
    dailymeasurementdata.h \
    energydatamanager.h

include(../qdbmanager/qdbmanager.pri)
include (../qthingspeak/qthingspeak.pri)
