import QtQuick 2.5

PinchArea{
    property double zoomFactor: 1.0
    property double dx
    property double dy
    anchors.fill: parent
    onPinchUpdated: {
        var dz = pinch.scale - pinch.previousScale
        zoomFactor = 1.0 + dz
    }
    MouseArea {
        property bool isPanning: false
        property double lastX: -1
        property double lastY: -1
        anchors.fill: parent
        onPressed: {
            isPanning = true
            lastX = mouse.x
            lastY = mouse.y
        }

        onReleased: {
            isPanning = false
        }

        onPositionChanged: {
            if (isPanning) {
                dx = mouse.x - lastX
                dy = mouse.y - lastY
                lastX = mouse.x
                lastY = mouse.y
            }
        }
    }
}
