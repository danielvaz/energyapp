#ifndef DAILYMEASUREMENTDATA_H
#define DAILYMEASUREMENTDATA_H

#include <QDateTime>
#include <QVariant>

#include "dailymeasurement.h"

class DailyMeasurementData
{
public:
    DailyMeasurementData();
    DailyMeasurementData(DailyMeasurement *d)
        : m_day(d->day())
        , m_power(d->power())
        , m_avgTemperature(d->avgTemperature())
        , m_avgLuminosity(d->avgLuminosity())
    {

    }

    QVariant data(int role) const;

private:
    QDateTime m_day;
    double m_power;
    double m_avgTemperature;
    double m_avgLuminosity;
};

#endif // DAILYMEASUREMENTDATA_H
