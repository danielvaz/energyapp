#include "dailymeasurementdata.h"
#include "dailymeasurementlistmodel.h"

DailyMeasurementData::DailyMeasurementData()
{

}

QVariant DailyMeasurementData::data(int role) const
{
    switch (role) {
    case DailyMeasurementListModel::Day:
        return m_day;
    case DailyMeasurementListModel::Power:
        return m_power;
    case DailyMeasurementListModel::AvgTemperature:
        return m_avgTemperature;
    case DailyMeasurementListModel::AvgLuminosity:
        return m_avgLuminosity;
    default:
        return QVariant();
    }
}
