#ifndef LISTMODELTEMPLATE_H
#define LISTMODELTEMPLATE_H

#include <QAbstractListModel>
#include <QMetaEnum>
#include <QVariant>

template<class T>
class ListModelTemplate : public QAbstractListModel {
public:

    ListModelTemplate() : QAbstractListModel()
    {}

    ~ListModelTemplate()
    {}

    enum Roles {};
    Q_ENUMS(Roles)

    QHash<int, QByteArray> roleNames() const Q_DECL_FINAL {
        QHash<int, QByteArray> roles;
        const int enumIdx = metaObject()->indexOfEnumerator("Roles");
        const QMetaEnum metaEnum = metaObject()->enumerator(enumIdx);
        const int keysCount = metaEnum.keyCount();

        for( int i = 0; i < keysCount; ++i ) {
            roles.insert(metaEnum.value(i), metaEnum.key(i));
        }

        return roles;
    }

    QVariant data(const QModelIndex &index, int role) const Q_DECL_FINAL {
        if (index.row() < int(m_items.size())) {
            return m_items.at(index.row()).data(role);
        }
        return QVariant();
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_FINAL {
        Q_UNUSED(parent);
        return int(m_items.size());
    }

    bool insertRows(const std::vector<T> &data) {
        if (data.size() == 0)
            return false;

        beginInsertRows(QModelIndex(), m_items.size(), m_items.size() + data.size() - 1);
        m_items.insert(m_items.end(), data.begin(), data.end());
        endInsertRows();
        return true;
    }

    bool insertRow(const T &data) {
        beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
        m_items.push_back(data);
        endInsertRows();
        return true;
    }

    bool removeRows(int row, int count = 0) {
        beginRemoveRows(QModelIndex(), row, row + count);
        m_items.erase(m_items.begin() + row, m_items.begin() + row + count + 1);
        endRemoveRows();
        return true;
    }

    T at(int index) const {
        if (index < int(m_items.size())) {
            return m_items.at(index);
        }
        return T();
    }

    bool clear() {
        return removeRows(0, rowCount() - 1);
    }

protected:
    std::vector<T> m_items;
};

#endif // LISTMODELTEMPLATE_H
