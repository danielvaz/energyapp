#include "dailymeasurementlistmodel.h"

DailyMeasurementListModel::DailyMeasurementListModel()
{

}

QVariantMap DailyMeasurementListModel::get(int row) const {
    if ( row < 0 || (row > int(m_items.size() - 1)) ) {
        return QVariantMap();
    }

    QHash<int,QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    while (i.hasNext()) {
        i.next();
        QModelIndex idx = index(row, 0);
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}
