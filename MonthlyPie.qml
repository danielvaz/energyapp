import QtQuick 2.5
import QtQuick.Controls 2.0
import QtCharts 2.1

ChartView {
    id: dataView
    property string dateFmt: "dd/MM"
    property date initialDate: new Date()
    property date finalDate: initialDate
    property double powerConsumption: 0
    property double tusd: powerConsumption * energyDataManager.energyTUSD
    property double te: powerConsumption * energyDataManager.energyTE
    property double cost: tusd + te
    property double icms: cost * 0.12
    property double pis: cost * 0.0089
    property double cofins: cost * 0.0413
    property double totalCost: cost + icms + pis + cofins
    antialiasing: true
    anchors.fill: parent
    legend {
        alignment: Qt.AlignRight
    }
    theme: ChartView.ChartThemeBlueIcy
    title: {
        if ( powerConsumption !== 0 ) {
            var period = "Período: " + Qt.formatDate(initialDate, dateFmt) + " à " + Qt.formatDate(finalDate, dateFmt)
            return period + " - " + powerConsumption.toFixed(2) + "kWh" + " - R$ " + totalCost.toFixed(2)
        }
        return ""
    }

    Component.onCompleted: {
        energyDataManager.dailyDataChanged.connect(updateSeries)
    }

    Component.onDestruction: {
        energyDataManager.dailyDataChanged.disconnect(updateSeries)
    }

    ZoomTool {
        onDxChanged: {
            dataView.scrollLeft(dx)
        }
        onDyChanged: {
            dataView.scrollUp(dy)
        }
        onZoomFactorChanged: {
            dataView.zoom(zoomFactor)
        }
    }

    PieSeries {
        id: luminosityPlot
        name: qsTr("Conta de Energia")

        PieSlice {
            label: "TUSD R$ " + tusd.toFixed(2) + " , " + (percentage * 100).toFixed(1) + "%"
            value: tusd
        }

        PieSlice {
            label: "TE R$ " + te.toFixed(2) + " , " + (percentage * 100).toFixed(1) + "%"
            value: te
        }

        PieSlice {
            label: "PIS R$ " + pis.toFixed(2) + " , " + (percentage * 100).toFixed(1) + "%"
            value: pis
        }

        PieSlice {
            label: "COFINS R$ " + cofins.toFixed(2) + " , " + (percentage * 100).toFixed(1) + "%"
            value: cofins
        }

        PieSlice {
            label: "ICMS R$ " + icms.toFixed(2) + " , " + (percentage * 100).toFixed(1) + "%"
            value: icms
        }
    }

    function updateSeries() {
        var dailyData = energyDataManager.dailyData
        powerConsumption = 0
        initialDate = dailyData.get(0).Day
        var length = dailyData.count
        finalDate = dailyData.get(length-1).Day
        for(var i=0; i<length; i++) {
            var el = dailyData.get(i)
            powerConsumption += el.Power
        }
    }
}
