#ifndef HOMEMEASUREMENT_H
#define HOMEMEASUREMENT_H

#include <QDateTime>
#include <QObject>

class HomeMeasurement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime timestamp READ timestamp WRITE setTimestamp NOTIFY timestampChanged)
    Q_PROPERTY(double current READ current WRITE setCurrent NOTIFY currentChanged)
    Q_PROPERTY(double temperature READ temperature WRITE setTemperature NOTIFY temperatureChanged)
    Q_PROPERTY(double luminosity READ luminosity WRITE setLuminosity NOTIFY luminosityChanged)

public:
    explicit HomeMeasurement(QObject *parent = 0);

    QDateTime timestamp() const
    {
        return m_timestamp;
    }

    double current() const
    {
        return m_current;
    }

    double temperature() const
    {
        return m_temperature;
    }

    double luminosity() const
    {
        return m_luminosity;
    }

    void setTimestamp(QDateTime timestamp)
    {
        if (m_timestamp == timestamp)
            return;

        m_timestamp = timestamp;
        emit timestampChanged(timestamp);
    }
    void setCurrent(double current)
    {
        if (m_current == current)
            return;

        m_current = current;
        emit currentChanged(current);
    }
    void setTemperature(double temperature)
    {
        if (m_temperature == temperature)
            return;

        m_temperature = temperature;
        emit temperatureChanged(temperature);
    }
    void setLuminosity(double luminosity)
    {
        if (m_luminosity == luminosity)
            return;

        m_luminosity = luminosity;
        emit luminosityChanged(luminosity);
    }

signals:
    void timestampChanged(QDateTime timestamp);
    void currentChanged(double current);
    void temperatureChanged(double temperature);
    void luminosityChanged(double luminosity);

private:
    QDateTime m_timestamp;
    double m_current;
    double m_temperature;
    double m_luminosity;
};

#endif // HOMEMEASUREMENT_H
