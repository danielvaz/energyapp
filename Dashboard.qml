import QtQuick 2.5
import QtQuick.Controls 2.0
import energy 1.0

Item {
    property double expenses: energyDataManager.powerConsumption * (energyDataManager.energyTE + energyDataManager.energyTUSD)
    property string graphic

    onGraphicChanged: {
        switch(graphic) {
        case "Daily":
            stack.clear()
            stack.push(daily)
            break;
        case "Monthly":
            stack.clear()
            stack.push(monthly)
            break;
        case "MonthlyPie":
            stack.clear()
            stack.push(monthlyPie)
        }
    }

    Flickable {
        id: dataInfo
        anchors {
            bottom: parent.bottom
            top: parent.top
        }
        contentHeight: column.height
        contentWidth: column.width
        ScrollIndicator.vertical: ScrollIndicator { }
        ScrollIndicator.horizontal: ScrollIndicator { }
        width: 250
        Column {
            id: column
            spacing: 10
            Row {
                spacing: 15
                Image {
                    source: "images/timestamp.png"
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: Qt.formatDateTime(energyDataManager.ts, "dd/MM/yy\nhh:mm:ss")
                }
            }

            Row {
                spacing: 15
                Image {
                    source: "images/current.png"
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: energyDataManager.current.toFixed(1) + " A"
                }
            }

            Row {
                spacing: 15
                Image {
                    source: "images/temperature_inside.png"
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: energyDataManager.temperature.toFixed(1) + " °C"
                }
            }

            Row {
                spacing: 15
                Image {
                    source: "images/luminosity.png"
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: energyDataManager.luminosity.toFixed(1) + " lm"
                }
            }

            Row {
                spacing: 15
                Image {
                    source: "images/power.png"
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: energyDataManager.powerConsumption.toFixed(2) + " kWh"
                }
            }

            Row {
                spacing: 15
                Image {
                    source: "images/expenses.png"
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: "R$ " + expenses.toFixed(2)
                }
            }

            ComboBox {
                id: combo
                implicitHeight: 80
                textRole: "key"
                model: ListModel {
                    ListElement { key: "Diário"; value: "Daily" }
                    ListElement { key: "Mensal"; value: "Monthly" }
                    ListElement { key: "Pizza Mensal"; value: "MonthlyPie" }
                }
                onCurrentIndexChanged: {
                    graphic = model.get(currentIndex).value
                }
            }

            Button {
                id: updateGraphic
                enabled: energyDataManager.api.state === ThingspeakRequest.Idle
                implicitHeight: 80
                text: qsTr("Atualizar")
                onClicked: {
                    switch(combo.currentIndex) {
                    case 0:
                        energyDataManager.getInstantaneousData()
                        break;
                    case 1:
                        energyDataManager.requestLastDays(daysToRequest)
                        break;
                    case 2:
                        energyDataManager.requestLastDays(daysToRequest)
                        break;
                    }
                }
            }
        }
    }
    StackView {
        id: stack
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: dataInfo.right
            right: parent.right
        }
    }

    DailyHistory {
        id: daily
        visible: false
    }

    MonthlyHistory {
        id: monthly
        visible: false
    }

    MonthlyPie {
        id: monthlyPie
        visible: false
    }
}
