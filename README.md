# EnergyApp #

This application is used to visualize public a thingspeak channel that stores the electrical current, temperature and luminosity.

It's developed using C++ and Qt.

It uses [qthingspeak](https://bitbucket.org/danielvaz/qthingspeak) in order to handle ThingSpeak API requests.

It uses [qdbmanager](https://bitbucket.org/danielvaz/qdbmanager) in order to handle sqlite database.

The icons is from [icons8](https://icons8.com/)