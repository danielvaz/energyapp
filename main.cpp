#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "thingspeakrequest.h"
#include "dailymeasurementlistmodel.h"
#include "energydatamanager.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    EnergyDataManager::registerQml();
    ThingspeakRequest::registerQml();
    qmlRegisterType<DailyMeasurementListModel>("daily.data", 1, 0, "DailyMeasurementListModel");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
