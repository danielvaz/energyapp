import QtQuick 2.5
import QtCharts 2.1

ChartView {
    id: dataView
    property date startDate
    property date endDate
    property int dataLength: 7
    readonly property string dateFmt: "hh:mm"
    title: qsTr("Dados")
    antialiasing: true

    Component.onCompleted: {
        energyDataManager.timestampChanged.connect(updateSeries)
    }

    Component.onDestruction: {
        energyDataManager.timestampChanged.disconnect(updateSeries)
    }

    ZoomTool {
        onDxChanged: {
            dataView.scrollLeft(dx)
        }
        onDyChanged: {
            dataView.scrollUp(dy)
        }
        onZoomFactorChanged: {
            dataView.zoom(zoomFactor)
        }
    }

    LineSeries {
        id: currentPlot
        name: qsTr("Corrente")
        axisX: DateTimeAxis {
            min: startDate
            max: endDate
            format: dateFmt
            tickCount: dataLength
        }

        axisY: ValueAxis {
            readonly property double perc: Math.abs(energyDataManager.maxCurrent-energyDataManager.minCurrent) * 2
            min: energyDataManager.minCurrent - perc
            max: energyDataManager.maxCurrent + perc
            color: currentPlot.color
        }
    }

    LineSeries {
        id: temperaturePlot
        name: qsTr("Temperatura")
        axisX: DateTimeAxis {
            min: startDate
            max: endDate
            visible: false
        }

        axisY: ValueAxis {
            readonly property double perc: Math.abs(energyDataManager.maxTemperature-energyDataManager.minTemperature)
            min: energyDataManager.minTemperature - perc
            max: energyDataManager.maxTemperature + perc
            color: temperaturePlot.color
        }
    }

    LineSeries {
        id: luminosityPlot
        name: qsTr("Luminosidade")
        axisX: DateTimeAxis {
            min: startDate
            max: endDate
            visible: false
        }

        axisY: ValueAxis {
            readonly property double perc: Math.abs(energyDataManager.maxLuminosity-energyDataManager.minLuminosity)
            min: energyDataManager.minLuminosity - perc
            max: energyDataManager.maxLuminosity + perc
            color: luminosityPlot.color
        }
    }

    function updateSeries() {
        var currents = energyDataManager.currents
        var temperatures = energyDataManager.temperatures
        var luminosities = energyDataManager.luminosities
        var timestamp = energyDataManager.timestamp
        if ( timestamp.length > 1 ) {
            startDate = energyDataManager.timestamp[0]
            endDate = energyDataManager.timestamp[energyDataManager.timestamp.length-1]
            currentPlot.clear()
            temperaturePlot.clear()
            luminosityPlot.clear()
            for (var i=0; i<timestamp.length; ++i) {
                currentPlot.append(timestamp[i], currents[i])
                temperaturePlot.append(timestamp[i], temperatures[i])
                luminosityPlot.append(timestamp[i], luminosities[i])
            }
        }
    }
}
