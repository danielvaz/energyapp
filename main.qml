import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import energy 1.0

Window {
    id: winRoot
    visible: true
    width: Screen.width
    height: Screen.height
    title: qsTr("Monitor Energético")
    color: "#F4F4F4"
    property int daysToRequest: 30

    EnergyDataManager {
        id: energyDataManager
        Component.onCompleted: {
            energyDataManager.api.channel = 92142
            getInstantaneousData()
        }
    }

    Dashboard {
        id: dash
        anchors {
            fill: parent
            margins: 15
        }
    }
    BusyIndicator {
        anchors.centerIn: parent
        running: energyDataManager.api.state !== ThingspeakRequest.Idle
        Text {
            anchors {
                left: parent.right
                verticalCenter: parent.verticalCenter
            }
            visible: parent.running
            text: "Getting day " + Qt.formatDate(energyDataManager.api.requestingDay, "dd/MM")
        }
    }
}
