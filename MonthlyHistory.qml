import QtQuick 2.5
import QtQuick.Controls 2.0
import QtCharts 2.1

ChartView {
    id: dataView
    property string dateFmt: "dd/MM"
    property double powerConsumption: 0
    property double cost: powerConsumption * (energyDataManager.energyTUSD + energyDataManager.energyTE)
    readonly property double icms: 0.12
    readonly property double pis: 0.0089
    readonly property double cofins: 0.0413
    property double totalCost: cost + cost * ( icms + pis + cofins )
    antialiasing: true
    anchors.fill: parent
    title: "Potência: " + powerConsumption.toFixed(1) + " kWh - Total: R$" + totalCost.toFixed(2)

    Component.onCompleted: {
        energyDataManager.dailyDataChanged.connect(updateSeries)
    }

    Component.onDestruction: {
        energyDataManager.dailyDataChanged.disconnect(updateSeries)
    }

    ZoomTool {
        onDxChanged: {
            dataView.scrollLeft(dx)
        }
        onDyChanged: {
            dataView.scrollUp(dy)
        }
        onZoomFactorChanged: {
            dataView.zoom(zoomFactor)
        }
    }

    BarSeries {
        id: currentPlot
        name: qsTr("Potencia")
        axisX: BarCategoryAxis {
            id: categoryAxis
            labelsFont:Qt.font({pointSize: 6})

            Component.onCompleted: {
                var strs = []
                var today = new Date()
                for(var i=winRoot.daysToRequest;i>0;i--) {
                    var d = new Date()
                    d.setDate(today.getDate() - i)
                    strs.push(Qt.formatDate(d, dateFmt))
                }
                categories = strs
            }
        }

        axisY: ValueAxis {
            min: 0
            max: 1
            color: {
                if ( currentPlot.count !=0 ) {
                    return currentPlot.at(0).color
                } else {
                    return "#cccccc"
                }
            }
        }
    }

    LineSeries {
        id: temperaturePlot
        name: qsTr("Temperatura")
        axisX: DateTimeAxis {
            id: temperatureAxis
            visible: false
        }

        axisY: ValueAxis {
            min: -5
            max: 5
            color: temperaturePlot.color
        }
    }

    LineSeries {
        id: luminosityPlot
        name: qsTr("Luminosidade")
        axisX: temperatureAxis

        axisY: ValueAxis {
            min: -5
            max: 5
            color: luminosityPlot.color
        }
    }


    function updateSeries() {
        var dailyData = energyDataManager.dailyData
        var length = dailyData.count
        currentPlot.clear()
        temperaturePlot.clear()
        luminosityPlot.clear()
        temperatureAxis.min = dailyData.get(0).Day
        temperatureAxis.max = dailyData.get(length-1).Day
        powerConsumption = 0
        var powers = []
        var days = []
        var maxPower = dailyData.get(0).Power
        var minAvgTemp = dailyData.get(0).AvgTemperature
        var maxAvgTemp = dailyData.get(0).AvgTemperature
        var minAvgLumi = dailyData.get(0).AvgLuminosity
        var maxAvgLumi = dailyData.get(0).AvgLuminosity
        for(var i=0; i<length; i++) {
            var el = dailyData.get(i)
            var _power = el.Power
            var _temp = el.AvgTemperature
            var _lumi = el.AvgLuminosity
            var _day = el.Day
            powers.push(_power)
            days.push(Qt.formatDateTime(_day, dateFmt))
            maxPower = Math.max(maxPower, _power)
            minAvgTemp = Math.min(minAvgTemp, _temp)
            maxAvgTemp = Math.max(maxAvgTemp, _temp)
            minAvgLumi = Math.min(minAvgLumi, _lumi)
            maxAvgLumi = Math.max(maxAvgLumi, _lumi)
            temperaturePlot.append(_day, _temp)
            luminosityPlot.append(_day, _lumi)
            powerConsumption += _power
        }
        temperaturePlot.axisY.min = minAvgTemp - Math.abs(maxAvgTemp) * 0.2
        temperaturePlot.axisY.max = maxAvgTemp + Math.abs(maxAvgTemp) * 0.2
        luminosityPlot.axisY.min = minAvgLumi - Math.abs(maxAvgLumi) * 0.2
        luminosityPlot.axisY.max = maxAvgLumi + Math.abs(maxAvgLumi) * 0.2
        currentPlot.append("Potencia", powers)
        currentPlot.axisY.max = maxPower + maxPower * 0.2
        categoryAxis.categories = days
    }
}
