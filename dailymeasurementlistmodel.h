#ifndef DAILYMEASUREMENTLISTMODEL_H
#define DAILYMEASUREMENTLISTMODEL_H

#include "dailymeasurementdata.h"
#include "listmodeltemplate.h"

class DailyMeasurementListModel : public ListModelTemplate<DailyMeasurementData>
{
    Q_OBJECT
    Q_ENUMS(Roles)
    Q_PROPERTY(int count READ count)
public:
    enum Roles {
        Day = Qt::UserRole + 1,
        Power,
        AvgTemperature,
        AvgLuminosity
    };
    DailyMeasurementListModel();

    Q_INVOKABLE QVariantMap get(int row) const;

    inline int count () const {
        return rowCount();
    }
};

#endif // DAILYMEASUREMENTLISTMODEL_H
